def get_reversed_string(string):
    reversed_phrase = ""

    for i in range(len(string)-1, -1, -1):
        reversed_phrase += string[i]

    return reversed_phrase


phrase = input("Phrase to revert: ")

print(get_reversed_string(phrase))

