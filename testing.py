from time import sleep

ASTERISK = "*"
SPACE = " "


def is_limit(n, square_size):
    return n == 0 or n == square_size - 1


def is_diagonal(i, j, square_size):
    return i == j or i + j == square_size - 1


def print_square(square_size):
    for i in range(square_size):
        for j in range(square_size):
            if is_limit(i, square_size) or is_limit(j, square_size) or is_diagonal(i, j, square_size):
                print(ASTERISK, end='')
            else:
                print(SPACE, end='')
        print()


for i in range(0, 30):
    print_square(i)
    sleep(1)
