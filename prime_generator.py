LIMIT = 100000


def is_prime(number):
    for i in range(2, number):
        if i**2 <= number:
            if number % i == 0:
                return False
    return True


for n in range(2, LIMIT):
    if is_prime(n):
        print(n, end=" ")