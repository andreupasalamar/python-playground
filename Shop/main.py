# Auxiliary classes
from classes import product, warehouse, menu


def add_item_details():
    identifier = input("ID of the new product: ")
    brand = input("Brand of the new product: ")
    model = input("Model of the new product: ")
    description = input("Description of the new produc: ")

    warehouse.add_item(product.Product(identifier, brand, model, description))


def delete_item_details():
    identifier = input("Product ID: ")
    warehouse.delete_item(identifier)


# Initialize warehouse and menu
warehouse = warehouse.Warehouse()
menu = menu.Menu()

# Show warehouse menu and select appropriate option
end = False

while not end:
    option = menu.warehouse_menu()
    if option == 1:
        print(warehouse.show_items())
    elif option == 2:
        add_item_details()
    elif option == 3:
        delete_item_details()
    elif option == 50:
        end = True
    else:
        print("Please, select a correct option")
