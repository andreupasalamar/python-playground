class Warehouse:
    items = []
    capacity = 50

    def __init__(self):
        pass

    def add_item(self, product):
        if len(self.items) < self.capacity:
            self.items.append(product)
    
    def delete_item(self, id):
        for item in self.items:
            if item.id == id:
                self.items.remove(item)

    def show_items(self):
        string_builder = ""

        for item in self.items:
            string_builder += "ID: " + item.id + ", "
            string_builder += "Brand: " + item.brand + ", "
            string_builder += "Model: " + item.model + ", "
            string_builder += "Descritpion: " + item.description + "\n"
        
        return string_builder