CURRENCY_EUR = [100, 50, 20, 10, 5, 2, 1, 0.5, 0.2, 0.1, 0.05, 0.02, 0.01]
MESSAGE_ASK_MONEY = "Diners: "
ERR_MONEY_FORMAT = "Error. El valor introduït no és numéric. Els diners han de ser numèrics"
END_STRING = "end"
MESSAGE_WELCOME = (
            "Hola! aquest programa et dona el canvi en monedes i bitllets del valor introduït. Introdueix \"" + END_STRING + "\" per acabar el programa")


def get_change(f_money):
    string_builder = ""

    for i in range(len(CURRENCY_EUR)):
        currency = CURRENCY_EUR[i]
        quantity = f_money // currency

        if quantity > 0:
            string_builder += str(currency) + " : " + str(int(quantity)) + "\n"

        f_money = float("{:.2f}".format((f_money % currency)))

    return string_builder


print(MESSAGE_WELCOME)

end = False

while not end:
    money = input(MESSAGE_ASK_MONEY)
    try:
        money = float(money)
        print(get_change(money))
    except ValueError:
        if money == END_STRING:
            end = True
        else:
            print(ERR_MONEY_FORMAT)
