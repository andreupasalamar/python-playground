import datetime
import random
from datetime import date
import inflect

# Sets of sequences
SEQ_OTHER = [1, 2, 3]
SEQ_WEDNESDAY = [1, 2, 5]
SEQ_THURSDAY = [1, 4, 5]
SEQ_FRIDAY = [3, 4, 5]

# Sets of times
TIMES = ["10:00", "10:30", "11:00", "11:30", "12:00", "12:30", "15:00", "15:30"]
TIMES_TT = ["10:00", "10:30", "11:30", "12:00", "12:30", "15:00", "15:30"]


# Function for retrieving next date given a number of days
def get_next_date(date_check, jump):
    return date_check + datetime.timedelta(jump)


# Function for getting the appropriate sequence and not grab weekend days
def get_sequence(weekday):
    if weekday == 2:
        return SEQ_WEDNESDAY
    elif weekday == 3:
        return SEQ_THURSDAY
    elif weekday == 4:
        return SEQ_FRIDAY
    else:
        return SEQ_OTHER


# Get a time frame for the meeting. Takes into account Tuesdays and Thursdays.
def get_time(date_check):
    if date_check.weekday() in [1, 3]:
        return random.choice(TIMES_TT)
    return random.choice(TIMES)


today = date.today()

# Get corresponding sequence
sequence = get_sequence(today.weekday())

# Generate an array of 6 meeting dates
days = [get_next_date(today, sequence[0]), get_next_date(today, sequence[0]), get_next_date(today, sequence[0]),
        get_next_date(today, sequence[1]), get_next_date(today, sequence[1]), get_next_date(today, sequence[2])]

p = inflect.engine()

# Output each meeting time frame.
for day in days:
    print(day.strftime("%A, %B"), p.ordinal(day.day), get_time(day))
