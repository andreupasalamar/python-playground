LIMIT = 20


def get_fibonacci_sequence(limit):
    sequence = [0, 1]

    for i in range(limit):
        sequence.append(sequence[-1] + sequence[-2])

    return sequence


print(get_fibonacci_sequence(LIMIT))